import java.util.LinkedList;


public class Path {
	private LinkedList<Vertex> vertices;
	
	public Path() {
		vertices = new LinkedList<>();
	}
	
	public void addVertex(Vertex v) {
		vertices.add(v);
	}
	
	public void addVertexToBeginning(Vertex v) {
		vertices.add(0, v);
	}
	
	public boolean isEmpty() {
		return vertices.isEmpty();
	}
	
	public Vertex getFirstVertex() {
		return vertices.getFirst();
	}
	
	public void removeFirstVertex() {
		vertices.removeFirst();
	}
	
	public LinkedList<Vertex> getVertices() {
		return vertices;
	}
	
	public String toString() {
		String s = "";
		
		for (int i = 0; i < vertices.size(); i++) {
			if (i < vertices.size() - 1) {
				s += vertices.get(i) + "-->";
			} else {
				s += vertices.get(i) + ".";
			}
		}
		
		return s;
	}
}
