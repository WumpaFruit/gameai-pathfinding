import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Dijkstra {
	public static List<PathfindingResult> pathfindDijkstra(Graph g, Vertex start) {
		List<PathfindingResult> resultList = new ArrayList<>(g.getNumVertices() - 1);
		
		for (Vertex v: g.getVertices()) {
			if (!v.equals(start)) {
				resultList.add(pathfindDijkstra(g, start, v));
			}
		}
		
		return resultList;
	}
	
	public static PathfindingResult pathfindDijkstra(Graph g, Vertex start, Vertex goal) {
		PathfindingResult result = new PathfindingResult();
		boolean goalFound = false;
		int verticesVisited = 0;
		long timer = System.nanoTime();
		
		if (g.getVertex(start.getName()) == null) {
			result.message = "Start vertex not in graph!";
			return result;
		}
		
		if (g.getVertex(goal.getName()) == null) {
			result.message = "Goal vertex not in graph!";
			return result;
		}
		
		//Distance from the start vertex to another vertex
		Map<Vertex, Double> costSoFar = new HashMap<>();
		//The predecessor of a vertex
		Map<Vertex, Vertex> predecessor = new HashMap<>();
		
		List<Vertex> openList = new ArrayList<>();
		
		costSoFar.put(start, 0.0);	//Distance from start vertex to start is 0
		predecessor.put(start, null);	//There is no predecessor of the start vertex (for our purposes)
		goal = g.getVertex(goal.getName());

		//Initialize distances and predecessors for every vertex in the graph
		for(Vertex v: g.getVertices()) {
			if (v != null) { //The first vertex in a DIMACSGraph is null for easier indexing
				if (!v.equals(start)) {
					costSoFar.put(v, Double.MAX_VALUE);
					predecessor.put(v, null);
				}
			}
		}
		
		//Add the start vertex to the open list
		openList.add(g.getVertex(start.getName()));
		
		while(!openList.isEmpty()) {
			Vertex lowestCostVertex = getMinDistanceVertex(openList, costSoFar);

			openList.remove(lowestCostVertex);
			lowestCostVertex.setOnClosedList(true);
			verticesVisited++;
			
			if (lowestCostVertex.equals(goal)) {
				goalFound = true;
				break;
			}
			
			//Get a list of adjacent edges, each with a from vertex equal to the lowest cost vertex
			List<Edge> adjacentEdges = g.getEdgesFrom(lowestCostVertex);
			
			//Remove adjacent edges where the to vertex has already been visited
			//filterEdges(adjacentEdges, openList);
			
			for (Edge e: adjacentEdges) {
				Vertex toVertex = e.getToVertex();

				//Calculate distance going through lowest cost vertex to edge
				double altDistance = costSoFar.get(lowestCostVertex) + e.getCost();
				
				//If this edge is lower than the lowest cost distance to edge, update path
				if (altDistance < costSoFar.get(toVertex)) {
					costSoFar.put(toVertex, altDistance);
					predecessor.put(toVertex, lowestCostVertex);
				}
				
				if (!openList.contains(toVertex) && !toVertex.isOnClosedList()) {
					openList.add(toVertex);
				}
			}
		}
		if (goalFound) {
			//Fill the results with useful information
			result.path = generatePath(predecessor, goal);
			result.cost = generateTotalCost(start, result.path, g);
			
		} else {
			result.message = "Goal not found";
		}
		
		result.numVerticesVisited = verticesVisited;
		result.runtime = System.nanoTime() - timer;
		
		return result;
	}
	
	private static Vertex getMinDistanceVertex(List<Vertex> vertexList, Map<Vertex, Double> dist) {
		Vertex v = null;
		double minDist = Double.MAX_VALUE;
		
		for (Vertex a: vertexList) {
			double distA = dist.get(a);
			if (distA < minDist) {
				v = a;
				minDist = distA;
			}
		}
		
		return v;
	}
	
	private static Path generatePath(Map<Vertex, Vertex> predecessor, Vertex goal) {
		Path p = new Path();
		Vertex current = goal;
		
		while (predecessor.get(current) != null) {
			p.addVertexToBeginning(current);
			current = predecessor.get(current);
		}
		
		return p;
	}
	
	/*private static double generateTotalCost(Vertex start, Path path, List<Edge> edgeList) {
		double cost = 0.0;
		List<Vertex> pathList = path.getVertices();
		
		if (!pathList.isEmpty()) {
			for (int i = 0, j = 1; j < pathList.size(); i++, j++) {
				//Find the edge along the vertices in the path
				Edge e = getEdgeFromTwoVertices(pathList.get(i), pathList.get(j), edgeList);
				//Add cost of edge to total cost
				cost += e.getCost();
			}
			
			//Add cost from start vertex to first vertex in path
			cost += getEdgeFromTwoVertices(start, pathList.get(0), edgeList).getCost();
		}
		
		return cost;
	}*/
	
	/*private static Edge getEdgeFromTwoVertices(Vertex from, Vertex to, List<Edge> edgeList) {
		Edge e = null;
		
		for (Edge edge: edgeList) {
			if (edge.getFromVertex().equals(from) && edge.getToVertex().equals(to)) {
				return edge;
			}
		}
		
		return e;
	}*/
	
	private static double generateTotalCost(Vertex start, Path path, Graph g) {
		double cost = 0.0;
		List<Vertex> pathList = path.getVertices();
		
		if (!pathList.isEmpty()) {
			for (int i = 0, j = 1; j < pathList.size(); i++, j++) {
				//Find the edge along the vertices in the path
				Edge e = g.getEdgeFromTwoVertices(pathList.get(i), pathList.get(j));
				//Add cost of edge to total cost
				cost += e.getCost();
			}
			
			//Add cost from start vertex to first vertex in path
			cost += g.getEdgeFromTwoVertices(start, pathList.get(0)).getCost();
		}
		
		return cost;
	}
}