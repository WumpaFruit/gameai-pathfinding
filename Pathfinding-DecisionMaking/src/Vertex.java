import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class Vertex implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8967110822463145471L;
	private String name;
	private double latitude;
	private double longitude;
	private List<Edge> outgoingEdges;
	private transient boolean visited;
	private transient boolean onClosedList;
	
	public Vertex(String name) {
		this.name = name;
		outgoingEdges = new ArrayList<>();
		visited = false;
		onClosedList = false;
	}
	
	public void addOutgoingEdge(Edge e) {
		outgoingEdges.add(e);
	}
	
	public String getName() {
		return name;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vertex other = (Vertex) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	public String toString() {
		return name;
	}
	
	public List<Edge> getOutgoingEdges() {
		return outgoingEdges;
	}

	public boolean isVisited() {
		return visited;
	}

	public void setVisited(boolean visited) {
		this.visited = visited;
	}

	public boolean isOnClosedList() {
		return onClosedList;
	}

	public void setOnClosedList(boolean onClosedList) {
		this.onClosedList = onClosedList;
	}	
}