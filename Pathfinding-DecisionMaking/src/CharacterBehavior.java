public abstract class CharacterBehavior {
	protected int mode;
	public enum Type { NONE, IDLE, SEEK };
	public abstract void update(int time);
	
	public int getMode() {
		return mode;
	}
	
	public void setMode(int newMode) {
		mode = newMode;
	}
}
