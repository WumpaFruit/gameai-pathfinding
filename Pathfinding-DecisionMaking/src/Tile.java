import processing.core.PApplet;


public class Tile {
	public static final short DEFAULT_COLOR_R = 0;
	public static final short DEFAULT_COLOR_G = 0;
	public static final short DEFAULT_COLOR_B = 0;
	
	private int x;
	private int y;
	private int width;
	private int height;
	private short colorR;
	private short colorG;
	private short colorB;
	private boolean isObstacle;
	
	public Tile(int x, int y, int width, int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		colorR = DEFAULT_COLOR_R;
		colorG = DEFAULT_COLOR_G;
		colorB = DEFAULT_COLOR_B;
		isObstacle = false;
	}
	
	public void draw(PApplet parent) {
		parent.stroke(colorR, colorG, colorB);
		parent.fill(colorR, colorG, colorB);
		parent.rect(x, y, width, height);
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public short getColorR() {
		return colorR;
	}

	public void setColorR(short colorR) {
		this.colorR = colorR;
	}

	public short getColorG() {
		return colorG;
	}

	public void setColorG(short colorG) {
		this.colorG = colorG;
	}

	public short getColorB() {
		return colorB;
	}

	public void setColorB(short colorB) {
		this.colorB = colorB;
	}

	public boolean isObstacle() {
		return isObstacle;
	}

	public void setObstacle(boolean isObstacle) {
		this.isObstacle = isObstacle;
	}
	
}