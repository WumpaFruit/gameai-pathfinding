import java.util.Random;


public class AStarHeuristics {
	public enum Type {HAVERSINE, HAVERSINETRIPLED, EUCLIDEAN, CONSTANT, RANDOM, MANHATTAN}

	private static final double CONSTANT_HEURISTIC_VALUE = -10;
	private static final double RAND_MAX_VALUE = 100;
	
	/**
	 * Haversine formula for calculating the distance between two points with a latitude and longitude.
	 * Adapted from http://stackoverflow.com/questions/120283/how-can-i-measure-distance-and-create-a-bounding-box-based-on-two-latitudelongi
	 * @param lat1 the latitude of the 1st location
	 * @param long1 the longitude of the 1st location
	 * @param lat2 the latitude of the 2nd location
	 * @param long2 the longitude of the 2nd location
	 * @return A heuristic of the distance between the two locations
	 */
	public static double latLongEuclideanHeuristic(double lat1, double long1, double lat2, double long2) {
		double earthRadius = 3958.75;
	    double dLat = Math.toRadians(lat2-lat1);
	    double dLng = Math.toRadians(long2-long1);
	    double sindLat = Math.sin(dLat / 2);
	    double sindLng = Math.sin(dLng / 2);
	    double a = Math.pow(sindLat, 2) + Math.pow(sindLng, 2)
	            * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));
	    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
	    double dist = earthRadius * c;

	    return dist;
	}
	
	/**
	 * Straight-line Euclidean heuristic
	 * @param x1 x coordinate of first node
	 * @param y1 y coordinate of first node
	 * @param x2 x coordinate of second node
	 * @param y2 y coordinate of second node
	 * @return
	 */
	public static double euclideanHeuristic(double x1, double y1, double x2, double y2) {
		double deltaX = x1 - x2;
		double deltaY = y1 - y2;
		
		return Math.sqrt(deltaX * deltaX + deltaY * deltaY);
	}
	
	public static double manhattanHeuristic(double x1, double y1, double x2, double y2) {
		double deltaX = x1 - x2;
		double deltaY = y1 - y2;
		
		return Math.abs(deltaX) + Math.abs(deltaY);
	}
	
	public static double constantHeuristic() {
		return CONSTANT_HEURISTIC_VALUE;
	}
	
	public static double randomHeuristic() {
		Random rand = new Random();
		return rand.nextDouble() * RAND_MAX_VALUE;
	}
	
	
}
