import java.awt.Color;
import java.util.LinkedList;

import processing.core.PApplet;
import processing.core.PVector;


@SuppressWarnings("serial")
public class IndoorPathfinding extends PApplet{
	private static final int WIDTH = 800;
	private static final int HEIGHT = 800;
	private TileGrid tileGrid;
	private Character c;
	private static final int BREADCRUMB_INTERVAL = 900;
	private static final float MAX_SPEED = 0.05f;
	private static final float RADIUS_OF_SATISFACTION = 5f;
	private int lastMillis;
	private LinkedList<Character> breadcrumbs;
	private int delay;
	private Color backgroundColor = new Color(255, 255, 255);
	private Graph g;
	private boolean showBreadcrumbs, showPath;
	
	public static void main(String[] args) {
		PApplet.main(new String[] { "--present", "IndoorPathfinding" });
	}
	
	public void setup() {
		size(WIDTH, HEIGHT);
		tileGrid = new TileGrid("tilegrid.txt");
		g = TileGraph.createGraph(tileGrid);
		((TileGraph) g).addCoordInfo(tileGrid);
		
		//c = new Character(tileGrid.getRandomLocation(), Math.PI / 2.0, CharacterBehavior.Type.SEEK, tileGrid);
		c = new Character(new PVector(WIDTH / 2, HEIGHT / 2), Math.PI / 2.0, CharacterBehavior.Type.SEEK, tileGrid);
		c.setColor(0, 0, 0);
		
		lastMillis = millis();
		breadcrumbs = new LinkedList<>();
		delay = 0;
		showBreadcrumbs = true;
		showPath = false;
	}
	
	public void draw() {
		background(backgroundColor.getRed(), backgroundColor.getGreen(), backgroundColor.getBlue());
		tileGrid.draw(this, backgroundColor);
		if (showBreadcrumbs) {
			for (Character crumb: breadcrumbs) {
				crumb.draw(this, true);
			}
		}
		c.draw(this, false);
		
		if (showPath) {
			Path p = c.getPath();
			if (p != null) {
				for (Vertex v: p.getVertices()) {
					int tileNum = Integer.parseInt(v.getName());
					PVector pos = tileGrid.getLocationOfTile(tileNum);
					fill(255, 0, 0);
					stroke(255, 0, 0);
					ellipse(pos.x - 2.5f, pos.y - 2.5f, 5, 5);
				}
			}
		}
		
		textSize(16);
		fill(255, 0, 0);
		if (showBreadcrumbs) {
			text("Breadcrumbs are enabled - Press B to toggle", 10, HEIGHT - 30);
		} else {
			text("Breadcrumbs are disabled - Press B to toggle", 10, HEIGHT - 30);
		}
		
		if (showPath) {
			text("Path is shown - Press P to toggle", 10, HEIGHT - 15);
		} else {
			text("Path is not shown - Press P to toggle", 10, HEIGHT - 15);
		}
		
		update();
	}
	
	public void update() {
		c.update(millis() - lastMillis);
		//Do not add breadcrumbs when boid is not moving
		if (Vector.doublesEqual(c.getVelocity().x, 0.0) && 
				Vector.doublesEqual(c.getVelocity().y, 0.0)) {
			delay += millis() - lastMillis;
		}
		lastMillis = millis();
		
		if (millis() >= (breadcrumbs.size() * BREADCRUMB_INTERVAL) + delay) {
			//System.out.println("Add breadcrumb");
			breadcrumbs.add(new Character(new PVector(c.getPosition().x, c.getPosition().y), c.getOrientation(), CharacterBehavior.Type.NONE, tileGrid));
		}
	}
	
	public void mousePressed() {
		long timer = System.currentTimeMillis();
		Path p = AStar.pathfindAStar(g, g.getVertex(Integer.toString(tileGrid.getTileNumFromLocation(c.getPosition().x, c.getPosition().y))), g.getVertex(Integer.toString(tileGrid.getTileNumFromLocation(mouseX, mouseY))), AStarHeuristics.Type.EUCLIDEAN, true).path;
		if (p != null) {
			c.setPath(p);
		}
		lastMillis += System.currentTimeMillis() - timer;
	}
	
	public void keyPressed() {
		if (key == 'b') {
			showBreadcrumbs = !showBreadcrumbs;
		}
		
		if (key == 'p') {
			showPath = !showPath;
		}
	}
	
	public static PVector velocityToTarget(PVector cLoc, PVector tLoc) {
		PVector velocity = PVector.sub(tLoc, cLoc);
		
		if (velocity.mag() < RADIUS_OF_SATISFACTION) {
			return new PVector(0, 0);
		}
		
		velocity.normalize();
		velocity.mult(MAX_SPEED);
		
		return velocity;
	}
}
