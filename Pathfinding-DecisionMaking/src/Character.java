import processing.core.PApplet;
import processing.core.PVector;

/**
 * A "boid" character on the screen.
 * @author Joseph Sankar
 *
 */
public class Character {
	private static final int DEFAULT_RADIUS = 5;
	private static final int ORIENTATION_SMOOTHING_FACTOR = 50;
	private static final double BOID_DRAW_ANGLE = Math.PI / 4.0;
	private static final float TARGET_RADIUS = 0.1f;
	
	private PVector position;
	private double orientation;
	private PVector velocity;
	private double rotation;
	private int radius;
	private float rColor;
	private float gColor;
	private float bColor;
	private int totalSmoothingCount;
	private int currentSmoothingCount;
	private boolean velocityChanged;
	private double velAngleDiff;
	private double desiredOrientation;
	private PVector target;
	private Path currentPath;
	private float mass;
	
	private CharacterBehavior behavior;
	
	public Character(PVector pos, CharacterBehavior.Type t) {
		this(pos, 0, new PVector(0, 0), 0, t, null);
	}
	
	public Character(PVector pos, double orientation, CharacterBehavior.Type t) {
		this(pos, orientation, new PVector(0, 0), 0, t, null);
	}
	public Character(PVector pos, double orientation, CharacterBehavior.Type t, TileGrid grid) {
		this(pos, orientation, new PVector(0, 0), 0, t, grid);
	}
	
	public Character(PVector position, double orientation,
			         PVector velocity, double rotation, CharacterBehavior.Type t, TileGrid grid) {
		this.position = position;
		this.orientation = orientation;
		this.velocity = velocity;
		this.rotation = rotation;
		this.radius = DEFAULT_RADIUS;
		rColor = 0;
		gColor = 0;
		bColor = 0;
		currentSmoothingCount = 1;
		totalSmoothingCount = ORIENTATION_SMOOTHING_FACTOR;
		velocityChanged = false;
		desiredOrientation = orientation;
		mass = 1;
		
		switch (t) {
		case NONE:
			behavior = new NoBehavior();
			break;
		case IDLE:
			behavior = new IdleBehavior();
			break;
		case SEEK:
			behavior = new SeekBehavior(grid);
			break;
		}
	}
	
	/**
	 * Draws the character.
	 * @param parent The applet where this character is being drawn
	 * @param breadcrumb Whether this character is a breadcrumb
	 */
	public void draw(PApplet parent, boolean breadcrumb) {
		PVector v1 = Vector.vectorFromRadians(orientation + BOID_DRAW_ANGLE);
		PVector v2 = Vector.vectorFromRadians(orientation - BOID_DRAW_ANGLE);
		PVector v4 = Vector.vectorFromRadians(orientation);
		v4.mult(radius * 2);
		PVector v3 = PVector.add(position, v4);
		//System.out.println(v3);
		v1.mult(radius);
		v2.mult(radius);
		v1 = PVector.add(position, v1);
		v2 = PVector.add(position, v2);
		parent.stroke(rColor, gColor, bColor);
		if (!breadcrumb) {
			parent.fill(rColor, gColor, bColor);
			parent.triangle(v1.x, v1.y, v2.x, v2.y,
			        v3.x, v3.y);
		} else {
			parent.noFill();
			parent.line(v1.x, v1.y, v3.x, v3.y);
			parent.line(v2.x, v2.y, v3.x, v3.y);
		}
		
		parent.ellipse(position.x, position.y, radius * 2, radius * 2);
		
		//System.out.println(position + " " + v1 + " " + v2 + " " + v3);
		//System.out.println(Main.vectorFromRadians(r))
	}
	
	public void update(int time) {
		behavior.update(time);
	}
	
	public void setColor(float r, float g, float b) {
		rColor = r;
		gColor = g;
		bColor = b;
	}

	public PVector getPosition() {
		return position;
	}

	public double getOrientation() {
		return orientation;
	}

	public PVector getVelocity() {
		return velocity;
	}

	public void setVelocity(PVector velocity) {
		this.velocity = velocity;
	}

	public double getRotation() {
		return rotation;
	}

	public void setRotation(double rotation) {
		this.rotation = rotation;
	}

	public PVector getTarget() {
		return target;
	}

	public void setTarget(PVector target) {
		this.target = target;
		currentSmoothingCount = 1;
	}
	
	public CharacterBehavior getBehavior() {
		return behavior;
	}
	
	public float getMass() {
		return mass;
	}
	
	public void setMass(float newMass) {
		mass = newMass;
	}	
	class SeekBehavior extends CharacterBehavior{
		private TileGrid grid;
		
		public SeekBehavior(TileGrid grid) {
			this.grid = grid;
			mode = 1;
		}

		@Override
		public void update(int time) {
			//Shall we pathfind?
			if (currentPath != null && target == null) {
				int tileNum = Integer.parseInt(currentPath.getFirstVertex().getName());
				target = grid.getLocationOfTile(tileNum);
			}
			
			//If there is a target to seek
			if (target != null) {
				velocity = IndoorPathfinding.velocityToTarget(position, target);
				
				//Whether velocity is close to zero
				if (PVector.sub(position, target).mag() < TARGET_RADIUS) {
					velocity = new PVector(0, 0);
					target = null;
					currentPath.removeFirstVertex();
					
					if (currentPath.isEmpty()) {
						currentPath = null;
					}
				}
				
				//Ensure velocity only changes once
				if (!velocityChanged && !Vector.doublesEqual(desiredOrientation, orientation) && 
						currentSmoothingCount == 1) {
					velocityChanged = true;
				}
			}
			
			//Update position
			if (mode == 1 || Vector.doublesEqual(orientation, desiredOrientation)) {
				PVector vMult = PVector.mult(velocity, time);
				position.add(vMult);
			} 
			
			desiredOrientation = Vector.radiansFromVector(velocity);
			
			//If there is no velocity, do not change orientation
			if (velocity.x == 0 && velocity.y == 0) {
				desiredOrientation = orientation;
			}
			
			if (!velocityChanged && !Vector.doublesEqual(desiredOrientation, orientation) && 
					currentSmoothingCount == 1) {
				velocityChanged = true;
			}
			
			if (velocityChanged){
				//Manually adjust orientation over a few frames
				
				if (desiredOrientation < 0 || (desiredOrientation == 0 && orientation > 0)) {
					desiredOrientation += Math.PI * 2;
				}
				velAngleDiff = ((float) desiredOrientation)- (float)orientation;
				
				if (Math.abs(velAngleDiff) > Math.PI) {
					float sign = (float)Math.signum(velAngleDiff) * -1.0f;
					velAngleDiff = (Math.PI * 2) - Math.abs(velAngleDiff);
					velAngleDiff *= sign;
				}

				velocityChanged = false;
			}
			
			if (!Vector.doublesEqual(velAngleDiff, 0.0)) {
				
				orientation += (1.0/totalSmoothingCount) * (velAngleDiff);
				if (orientation >= Math.PI * 2) {
					orientation -= Math.PI * 2;
				}
				
				if (orientation < 0) {
					orientation += Math.PI * 2;
				}

				if (currentSmoothingCount == totalSmoothingCount) {
					currentSmoothingCount = 1;
					orientation = desiredOrientation;
					velAngleDiff = 0.0;
				} else {
					currentSmoothingCount++;
				}
			}
			
			//orientation += rotation * time;
			
		}

	}
	
	class IdleBehavior extends CharacterBehavior{

		@Override
		public void update(int time) {
			desiredOrientation = Vector.radiansFromVector(velocity);
			
			//If there is no velocity, do not change orientation
			if (velocity.x == 0 && velocity.y == 0) {
				desiredOrientation = orientation;
			}
			
			if (!velocityChanged && !Vector.doublesEqual(desiredOrientation, orientation) && 
					currentSmoothingCount == 1) {
				velocityChanged = true;
			}
			
			PVector vMult = PVector.mult(velocity, time);
			position.add(vMult);
			
			if (velocityChanged){
				//Manually adjust orientation over a few frames
				
				if (desiredOrientation < 0 || (desiredOrientation == 0 && orientation > 0)) {
					desiredOrientation += Math.PI * 2;
				}
				velAngleDiff = ((float) desiredOrientation)- (float)orientation;
				
				if (Math.abs(velAngleDiff) > Math.PI) {
					//System.out.print(Math.toDegrees(velAngleDiff) + "--> ");
					float sign = (float)Math.signum(velAngleDiff) * -1.0f;
					velAngleDiff = (Math.PI * 2) - Math.abs(velAngleDiff);
					velAngleDiff *= sign;
					//System.out.println(Math.toDegrees(velAngleDiff));
				}
				//System.out.println(velAngleDiff);
				velocityChanged = false;
				//System.out.println("Calculating angle diff to be " + newO + " - " + orientation);
			}
			
			if (!Vector.doublesEqual(velAngleDiff, 0.0)) {
				
				orientation += (1.0/totalSmoothingCount) * (velAngleDiff);
				if (orientation >= Math.PI * 2) {
					orientation -= Math.PI * 2;
				}
				
				if (orientation < 0) {
					orientation += Math.PI * 2;
				}

				if (currentSmoothingCount == totalSmoothingCount) {
					currentSmoothingCount = 1;
					orientation = desiredOrientation;
					velAngleDiff = 0.0;
				} else {
					currentSmoothingCount++;
				}
			}
			
			//orientation += rotation * time;
			
		}

	}
	
	class NoBehavior extends CharacterBehavior{

		@Override
		public void update(int time) { }
		
	}
	public void setPath(Path p) {
		currentPath = p;
	}
	
	public Path getPath() {
		return currentPath;
	}
}