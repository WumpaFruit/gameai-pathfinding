import processing.core.PVector;


public class Steering {
	PVector linear = new PVector(0, 0);
	float angular;
	
	public void add(Steering other) {
		linear.add(other.linear);
		angular += other.angular;
	}
	
	public Steering mult(float scalar) {
		linear = PVector.mult(linear, scalar);
		angular *= scalar;
		return this;
	}
}
