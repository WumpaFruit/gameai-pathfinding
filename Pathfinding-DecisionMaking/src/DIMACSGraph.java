import java.io.File;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;


/**
 * A graph where vertex names are integer IDs.
 * @author Joseph Sankar
 *
 */
@SuppressWarnings("serial")
public class DIMACSGraph extends Graph implements Serializable{
	private Vertex[] vertices;
	
	public DIMACSGraph(int numVertices, int numEdges) {
		super(numVertices, numEdges);
		vertices = new Vertex[numVertices + 1];
	}
	
	public static DIMACSGraph createGraphFromGR(String filename) {
		DIMACSGraph g = null;
		Scanner input = null;
		
		try {
			input = new Scanner(new File(filename));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (input != null) {
			while (input.hasNextLine()) {
				String line = input.nextLine();
				Scanner lineScan = new Scanner(line);
				lineScan.useDelimiter(" ");
				String prefix = lineScan.next();
				
				if (prefix.equals("c")) {	//Ignore comment lines
					if (lineScan.hasNextLine()) {
						lineScan.nextLine();
					}
				} else if (prefix.equals("p")) {
					lineScan.next();
					int numVertices = lineScan.nextInt();
					int numEdges = lineScan.nextInt();
					g = new DIMACSGraph(numVertices, numEdges);
					System.out.println("Num vertices: " + numVertices);
					System.out.println("Num edges: " + numEdges);
				} else if (prefix.equals("a")) {
					String fromVertex = lineScan.next();
					String toVertex = lineScan.next();
					double edgeWeight = lineScan.nextDouble();
					
					Edge e = new Edge(new Vertex(fromVertex), new Vertex(toVertex), edgeWeight);
					g.addEdge(e);
				} else {
					lineScan.close();
					input.close();
					throw new InputMismatchException(filename + " not formatted properly");
				}
				
				lineScan.close();
			}
			
			input.close();
		}
		
		return g;
	}
	
	/**
	 * Adds XY coordinate information to the graph
	 * @param filename the name of the file containing coordinate information
	 */
	public void addCoordInfo(String filename) {
		Scanner input = null;
		
		try {
			input = new Scanner(new File(filename));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (input != null) {
			while (input.hasNextLine()) {
				String line = input.nextLine();
				Scanner lineScan = new Scanner(line);
				lineScan.useDelimiter(" ");
				String prefix = lineScan.next();
				
				if (prefix.equals("c")) {
					if (lineScan.hasNextLine()) {
						lineScan.nextLine();
					}
				} else if (prefix.equals("p")) {
					lineScan.next();
					lineScan.next();
					lineScan.next();
					int numLines = lineScan.nextInt();
					if (numLines != vertices.length - 1) {
						lineScan.close();
						input.close();
						throw new InputMismatchException("Unequal amount of coordinate lines (" + numLines + "!=" + (vertices.length - 1) + ")");
					}
				} else if (prefix.equals("v")) {
					String vertexName = lineScan.next();
					long latitude = lineScan.nextLong();
					long longitude = lineScan.nextLong();
					Vertex v = vertices[Integer.parseInt(vertexName)];
					
					if (v == null) {
						lineScan.close();
						input.close();
						throw new InputMismatchException("Vertex " + vertexName + " not in graph!");
					}
					
					v.setLatitude(latitude);
					v.setLongitude(longitude);
				} else {
					lineScan.close();
					input.close();
					throw new InputMismatchException(filename + " not formatted properly");
				}
				
				lineScan.close();
			}
			
			input.close();
		}
	}
	
	/**
	 * Adds the provided edge to the graph and adds the vertices if they don't already exist.
	 * @param e the edge to add
	 */
	public void addEdge(Edge e) {
		getEdges().add(e);
		
		e.getFromVertex().addOutgoingEdge(e);
		
		addVertex(e.getFromVertex());
		addVertex(e.getToVertex());
		
	}
	
	public void addVertex(Vertex v) {
		int index = Integer.parseInt(v.getName());
		if (vertices[index] == null) {
			vertices[index] = v;
		} else {
			vertices[index].getOutgoingEdges().addAll(v.getOutgoingEdges());
		}
	}
	
	public List<Edge> getEdgesFrom(Vertex v) {
		return vertices[Integer.parseInt(v.getName())].getOutgoingEdges();
	}
	
	public Edge getEdgeFromTwoVertices(Vertex from, Vertex to) {
		int fromID = Integer.parseInt(from.getName());
		
		for (Edge edge: vertices[fromID].getOutgoingEdges()) {
			if (edge.getToVertex().equals(to)) {
				return edge;
			}
		}
		
		return null;
	}
	
	public Vertex getVertex(String name) {
		return vertices[Integer.parseInt(name)];
	}
	
	public int getNumVertices() {
		return vertices.length - 1;
	}
	
	public List<Vertex> getVertices() {
		return Arrays.asList(vertices);
	}
}
