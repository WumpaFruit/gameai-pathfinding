import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


@SuppressWarnings("serial")
public class TileGraph extends Graph{
	private Vertex[][] vertices;
	
	public TileGraph(int numRows, int numCols) {
		vertices = new Vertex[numRows][numCols];
	}
	
	public static TileGraph createGraph(TileGrid grid) {
		int numRows = grid.getNumHorizontalTiles();
		int numCols = grid.getNumVerticalTiles();
		TileGraph g = new TileGraph(numRows, numCols);
		
		for (int i = 0; i < numRows; i++) {
			for (int j = 0; j < numCols; j++) {
				if (grid.get(i, j) != null) {
					Vertex from = new Vertex(Integer.toString(i * numCols + j));
					
					if (grid.get(i-1, j-1) != null && !grid.get(i-1, j-1).isObstacle()) {
						Vertex to = new Vertex(Integer.toString((i-1) * numCols + (j-1)));
						g.addVertex(to);
						//g.addEdge(new Edge(from, to, Math.sqrt(2)));
					}
					
					if (grid.get(i, j-1) != null && !grid.get(i, j-1).isObstacle()) {
						Vertex to = new Vertex(Integer.toString((i) * numCols +  (j-1)));
						g.addEdge(new Edge(from, to, 1));
					}
					
					if (grid.get(i+1, j-1) != null && !grid.get(i+1, j-1).isObstacle()) {
						Vertex to = new Vertex(Integer.toString((i+1) * numCols + (j-1)));
						g.addVertex(to);
						//g.addEdge(new Edge(from, to, Math.sqrt(2)));
					}
					
					if (grid.get(i-1, j) != null && !grid.get(i-1, j).isObstacle()) {
						Vertex to = new Vertex(Integer.toString((i-1) * numCols + (j)));
						g.addEdge(new Edge(from, to, 1));
					}
					
					if (grid.get(i+1, j) != null && !grid.get(i+1, j).isObstacle()) {
						Vertex to = new Vertex(Integer.toString((i+1) * numCols + (j)));
						g.addEdge(new Edge(from, to, 1));
					}
					
					if (grid.get(i-1, j+1) != null && !grid.get(i-1, j+1).isObstacle()) {
						Vertex to = new Vertex(Integer.toString((i-1) * numCols + (j+1)));
						g.addVertex(to);
						//g.addEdge(new Edge(from, to, Math.sqrt(2)));
					}
					
					if (grid.get(i, j+1) != null && !grid.get(i, j+1).isObstacle()) {
						Vertex to = new Vertex(Integer.toString((i) * numCols + (j+1)));
						g.addEdge(new Edge(from, to, 1));
					}
					
					if (grid.get(i+1, j+1) != null && !grid.get(i+1, j+1).isObstacle()) {
						Vertex to = new Vertex(Integer.toString((i+1) * numCols + (j+1)));
						g.addVertex(to);
						//g.addEdge(new Edge(from, to, Math.sqrt(2)));
					}
				}
			}
		}
		
		return g;
	}
	
	public void addCoordInfo(TileGrid grid) {
		int numRows = vertices.length;
		int numCols = vertices[0].length;
		for (int i = 0; i < numRows; i++) {
			for (int j = 0; j < numCols; j++) {
				if (vertices[i][j] != null) {
					vertices[i][j].setLatitude(grid.get(i, j).getX());
					vertices[i][j].setLongitude(grid.get(i, j).getY());
				}
			}
		}
	}

	public void addEdge(Edge edge) {
		addVertex(edge.getFromVertex());
		addVertex(edge.getToVertex());
		
		getEdges().add(edge);
		getVertex(edge.getFromVertex().getName()).addOutgoingEdge(edge);
	}
	
	public void addVertex(Vertex v) {
		int tileNum = Integer.parseInt(v.getName());
		int tileRow = tileNum / vertices[0].length;
		int tileCol = tileNum % vertices[0].length;
		
		if (vertices[tileRow][tileCol] == null) {
			vertices[tileRow][tileCol] = v;
		}
	}
	
	public List<Edge> getEdgesFrom(Vertex v) {
		int tileNum = Integer.parseInt(v.getName());
		int tileRow = tileNum / vertices[0].length;
		int tileCol = tileNum % vertices[0].length;
		
		return vertices[tileRow][tileCol].getOutgoingEdges();
	}
	
	public Edge getEdgeFromTwoVertices(Vertex from, Vertex to) {
		
		for (Edge edge: getEdgesFrom(from)) {
			if (edge.getToVertex().equals(to)) {
				return edge;
			}
		}
		
		return null;
	}
	
	public Vertex getVertex(String name) {
		int tileNum = Integer.parseInt(name);
		int tileRow = tileNum / vertices[0].length;
		int tileCol = tileNum % vertices[0].length;
		
		return vertices[tileRow][tileCol];
	}
	
	public int getNumVertices() {
		return vertices.length * vertices[0].length;
	}
	
	public List<Vertex> getVertices() {
		List<Vertex> vertexList = new ArrayList<>();
		
		for (Vertex[] vertexArray: vertices) {
			vertexList.addAll(Arrays.asList(vertexArray));
		}
		
		return vertexList;
	}
}
