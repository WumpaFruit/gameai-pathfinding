import java.io.File;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class Graph implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6215900940708839519L;
	private List<Edge> edgeList;
	private List<Vertex> vertexList;
	
	public Graph() {
		edgeList = new ArrayList<>();
		vertexList = new ArrayList<>();
	}
	
	public Graph(int numVertices, int numEdges) {
		edgeList = new ArrayList<>(numEdges);
		vertexList = new ArrayList<>(numVertices);
	}
	
	public void addEdge(Edge e) {
		edgeList.add(e);
		
		Vertex fromVertex = getVertex(e.getFromVertex().getName());
		
		if (fromVertex == null) {	//From vertex isn't in vertex list
			fromVertex = e.getFromVertex();
			vertexList.add(fromVertex);
		} 
		
		fromVertex.addOutgoingEdge(e);
		
		Vertex toVertex = getVertex(e.getToVertex().getName());
		
		if (toVertex == null) {	//To vertex isn't in vertex list
			toVertex = e.getToVertex();
			vertexList.add(toVertex);
		} 
	}
	
	public List<Edge> getEdgesFrom(Vertex v) {
		return getVertex(v.getName()).getOutgoingEdges();
	}
	
	public static Graph createGraph(String filename) {
		Graph g = null;
		Scanner input = null;
		
		try {
			input = new Scanner(new File(filename));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (input != null) {
			g = new Graph();
			while (input.hasNextLine()) {
				String line = input.nextLine();
				Scanner lineScan = new Scanner(line);
				lineScan.useDelimiter(",");
				
				String fromVertex = lineScan.next();
				String toVertex = lineScan.next();
				double edgeWeight = lineScan.nextDouble();
				
				Edge e = new Edge(new Vertex(fromVertex), new Vertex(toVertex), edgeWeight);
				g.addEdge(e);
				
				lineScan.close();
			}
			
			input.close();
		}
		
		System.out.println("Num vertices: " + g.getNumVertices());
		System.out.println("Num edges: " + g.getEdges().size());
		
		return g;
	}
	
	/**
	 * Adds latitude and longitude information to the vertices from the specified file
	 * @param filename the name of the file containing latitude and longitude information for each vertex
	 */
	public void addLatitudeLongitudeInfo(String filename) {
		Scanner input = null;
		
		try {
			input = new Scanner(new File(filename));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (input != null) {
			while (input.hasNextLine()) {
				String line = input.nextLine();
				Scanner lineScan = new Scanner(line);
				lineScan.useDelimiter(",");
				
				String vertexName = lineScan.next();
				double latitude = lineScan.nextDouble();
				double longitude = lineScan.nextDouble();
				
				Vertex v = getVertex(vertexName);
				
				if (v == null) {
					lineScan.close();
					input.close();
					throw new InputMismatchException("Vertex not in graph!");
				}
				
				v.setLatitude(latitude);
				v.setLongitude(longitude);
				
				lineScan.close();
			}
			
			input.close();
		}
	}
	
	public Vertex getVertex(String name) {
		for (Vertex v: vertexList) {
			if (v.getName().equals(name)) {
				return v;
			}
		}
		
		return null;
	}
	
	public Edge getEdgeFromTwoVertices(Vertex from, Vertex to) {		
		for (Edge edge: edgeList) {
			if (edge.getFromVertex().equals(from) && edge.getToVertex().equals(to)) {
				return edge;
			}
		}
		
		return null;
	}
	
	public String toString() {
		String s = "";
		
		for (Edge e: edgeList) {
			s += e + "\n";
		}
		
		return s;
	}
	
	public List<Vertex> getVertices() {
		return vertexList;
	}
	
	public int getNumVertices() {
		return vertexList.size();
	}
	
	public List<Edge> getEdges() {
		return edgeList;
	}
}
