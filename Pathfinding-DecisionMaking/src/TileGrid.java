import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

import processing.core.PApplet;
import processing.core.PVector;

public class TileGrid {
	private Tile[][] tiles;
	private int tileWidth;
	private int tileHeight;
	private int numHorizontalTiles;
	private int numVerticalTiles;
	
	public TileGrid(int canvasWidth, int canvasHeight, int tileWidth, int tileHeight) {
		this.tileWidth = tileWidth;
		this.tileHeight = tileHeight;
		numHorizontalTiles = canvasWidth/tileWidth;
		numVerticalTiles = canvasHeight/tileHeight;
		tiles = new Tile[numHorizontalTiles][numVerticalTiles];
	}
	
	public TileGrid(String filename) {
		Scanner input = null;
		
		try {
			input = new Scanner(new File(filename));
			
			numHorizontalTiles = input.nextInt();
			numVerticalTiles = input.nextInt();
			tileWidth = input.nextInt();
			tileHeight = input.nextInt();
			input.nextLine();
			
			tiles = new Tile[numHorizontalTiles][numVerticalTiles];
			
			while (input.hasNextLine()) {
				String line = input.nextLine();
				if (line.startsWith("//")) { //Ignore comments
					continue;
				}
				Scanner lineScan = new Scanner(line);
				
				int tileX = lineScan.nextInt();
				int tileY = lineScan.nextInt();
				boolean obstacle = lineScan.nextBoolean();
				short colorR = lineScan.nextShort();
				short colorG = lineScan.nextShort();
				short colorB = lineScan.nextShort();
				
				tiles[tileX][tileY] = new Tile(tileX * tileWidth, tileY * tileHeight, tileWidth, tileHeight);
				tiles[tileX][tileY].setObstacle(obstacle);
				tiles[tileX][tileY].setColorR(colorR);
				tiles[tileX][tileY].setColorG(colorG);
				tiles[tileX][tileY].setColorB(colorB);
				lineScan.close();
			}
			
			input.close();
		} catch (IOException e) {
			e.printStackTrace();
		} 
		
		for (int i = 0; i < numHorizontalTiles; i++) {
			for (int j = 0; j < numVerticalTiles; j++) {
				if (tiles[i][j] == null) {
					tiles[i][j] = new Tile(i * tileWidth, j * tileHeight, tileWidth, tileHeight);
					tiles[i][j].setObstacle(false);
					tiles[i][j].setColorR((short) 255);
					tiles[i][j].setColorG((short) 255);
					tiles[i][j].setColorB((short) 255);
				}
			}
		}
	}
	
	public Tile getFromTileNum(int tileNum) {
		int tileRow = tileNum / numHorizontalTiles;
		int tileCol = tileNum % numHorizontalTiles;
		
		return get(tileRow, tileCol);
	}
	
	public Tile getFromLocation(float x, float y) {
		int tileRow = (int)x / tileWidth;
		int tileCol = (int)y / tileHeight;
		
		return get(tileRow, tileCol);
	}
	
	public Tile get(int row, int col) {
		if (row < 0 || col < 0 || row >= numHorizontalTiles || col >= numVerticalTiles) {
			return null;
		}
		return tiles[row][col];
	}
	
	public int getTileNumFromLocation(float x, float y) {
		int tileRow = (int)x / tileWidth;
		int tileCol = (int)y / tileHeight;
		
		return tileRow * numVerticalTiles + tileCol;
	}
	
	public PVector getLocationOfTile(int tileNum) {
		Tile t = getFromTileNum(tileNum);
		
		return new PVector(t.getX(), t.getY());
	}
	
	public PVector getRandomLocation() {
		Random rand = new Random();
		int randX = rand.nextInt(numHorizontalTiles);
		int randY = rand.nextInt(numVerticalTiles);
		Tile t = get(randX, randY);
		
		while (t.isObstacle()) {
			randX = rand.nextInt(numHorizontalTiles);
			randY = rand.nextInt(numVerticalTiles);
			t = get(randX, randY);
		}
		
		return new PVector(t.getX(), t.getY()); 
	}
	
	public void draw(PApplet parent, Color backgroundColor) {
		for (Tile[] tileRow: tiles) {
			for (Tile t: tileRow) {
				//Do not draw the tile if it is the same color as the background
				if (t != null && (backgroundColor.getRed() != t.getColorR() || backgroundColor.getGreen() != t.getColorG() || backgroundColor.getBlue() != t.getColorB())) {
					t.draw(parent);
				}
			}
		}
	}

	public int getNumHorizontalTiles() {
		return numHorizontalTiles;
	}

	public int getNumVerticalTiles() {
		return numVerticalTiles;
	}
}
