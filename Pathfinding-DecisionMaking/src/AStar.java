import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class AStar {
	
	/**
	 * Performs the A* pathfinding algorithm on the provided graph and start and goal vertices
	 * @param g the graph
	 * @param start the start vertex
	 * @param goal the goal vertex
	 * @return A pathfinding result of the A* algorithm
	 */
	public static PathfindingResult pathfindAStar(Graph g, Vertex start, Vertex goal, AStarHeuristics.Type heuristicType, boolean performLowestCostCheck) {
		PathfindingResult result = new PathfindingResult();
		boolean goalFound = false;
		int verticesVisited = 0;
		long timer = System.nanoTime();
		Runtime runtime = Runtime.getRuntime();
		runtime.gc();		//Run the garbage collector before beginning
		Vertex possibleGoal = null;
		
		if (g.getVertex(start.getName()) == null) {
			result.message = "Start vertex not in graph!";
			return result;
		}
		
		if (g.getVertex(goal.getName()) == null) {
			result.message = "Goal vertex not in graph!";
			return result;
		}
		
		//Distance from the start vertex to another vertex
		Map<Vertex, Double> costSoFar = new HashMap<>();
		
		//Value of the heuristic function for a particular vertex
		Map<Vertex, Double> heuristic = new HashMap<>();
		
		//The predecessor of a vertex
		Map<Vertex, Vertex> predecessor = new HashMap<>();
		
		//Create the open and closed lists
		List<Vertex> openList = new ArrayList<>();
		//List<Vertex> closedList = new ArrayList<>();
		
		costSoFar.put(start, 0.0);
		heuristic.put(start, 0.0);
		predecessor.put(start, null);
		goal = g.getVertex(goal.getName()); //Obtain heuristic information from the graph
		
		//Initialize distances and predecessors for every vertex in the graph
		for(Vertex v: g.getVertices()) {
			if (v != null) {
				if (!v.equals(start)) {
					costSoFar.put(v, Double.MAX_VALUE);
					heuristic.put(v, Double.MAX_VALUE);
					predecessor.put(v, null);
					v.setOnClosedList(false);
					v.setVisited(false);
				}
			}
		}
		
		//Add the start vertex to the open list
		openList.add(g.getVertex(start.getName()));
		
		while (!openList.isEmpty()) {
			Vertex lowestCostVertex = getMinCostVertex(openList, costSoFar, heuristic);
			openList.remove(lowestCostVertex);

			lowestCostVertex.setOnClosedList(true);
			
			if (!lowestCostVertex.isVisited()) {
				verticesVisited++;
				lowestCostVertex.setVisited(true);
			}
			
			if (lowestCostVertex.equals(goal)) {
				possibleGoal = lowestCostVertex;
				if (!performLowestCostCheck) {
					goalFound = true;
					break;
				}
			}
			
			if (possibleGoal != null) {
				if (getMinCostSoFar(openList, costSoFar) >= costSoFar.get(possibleGoal)) {
					goalFound = true;
					break;
				}
			}
			
			//Get a list of adjacent edges, each with a from vertex equal to the lowest cost vertex
			List<Edge> adjacentEdges = g.getEdgesFrom(lowestCostVertex);
			
			for (Edge e: adjacentEdges) {
				Vertex toVertex = g.getVertex(e.getToVertex().getName());
				double toVertexCost = costSoFar.get(lowestCostVertex) + e.getCost();
				
				//If the to vertex is already on the closed list, see if we just found a 
				//lower cost path to it
				if (toVertex.isOnClosedList()) {
					
					//Skip if we haven't found a lower cost path
					if (costSoFar.get(toVertex) <= toVertexCost) {
						continue;
					}
					
					//Otherwise, remove it from the closed list
					toVertex.setOnClosedList(false);
				} else if (openList.contains(toVertex)) { //Skip if we haven't found a lower cost path
					
					if (costSoFar.get(toVertex) <= toVertexCost) {
						continue;
					}
					
					
				} else {	//We have reached an unvisited node, update heuristic
					updateHeuristic(toVertex, goal, heuristic, heuristicType);
					costSoFar.put(toVertex, costSoFar.get(lowestCostVertex) + e.getCost());
				}
				
				//Add the to vertex to the open list
				if (!openList.contains(toVertex)) {
					openList.add(toVertex);
				}
				
				//Store the best predecessor (so far) so we can backtrack later to form a path
				predecessor.put(toVertex, lowestCostVertex);
			}			
		}
		
		if (goalFound) {
			//Fill the results with useful information
			result.path = generatePath(predecessor, possibleGoal);
			result.cost = generateTotalCost(start, result.path, g);
			
		} else {
			result.message = "Goal not found";
		}
		
		result.numVerticesVisited = verticesVisited;
		result.runtime = System.nanoTime() - timer;
		result.usedMemory = runtime.totalMemory() - runtime.freeMemory();
		
		return result;
	}
	
	private static void updateHeuristic(Vertex v, Vertex goal, Map<Vertex, Double> heuristic, AStarHeuristics.Type heuristicType) {
		switch (heuristicType) {
			case HAVERSINE:
				double haversineValue = AStarHeuristics.latLongEuclideanHeuristic(v.getLatitude(), v.getLongitude(), goal.getLatitude(), goal.getLongitude());
				heuristic.put(v, haversineValue);
				break;
			case HAVERSINETRIPLED:
				double haversineTripledValue = AStarHeuristics.latLongEuclideanHeuristic(v.getLatitude(), v.getLongitude(), goal.getLatitude(), goal.getLongitude()) * 3;
				heuristic.put(v, haversineTripledValue);
				break;
			case EUCLIDEAN:
				double euclideanValue = AStarHeuristics.euclideanHeuristic(v.getLatitude(), v.getLongitude(), goal.getLatitude(), goal.getLongitude());
				heuristic.put(v, euclideanValue);
				break;
			case MANHATTAN:
				double manhattanValue = AStarHeuristics.manhattanHeuristic(v.getLatitude(), v.getLongitude(), goal.getLatitude(), goal.getLongitude());
				heuristic.put(v, manhattanValue);
				break;
			case CONSTANT:
				double constantHeuristicValue = AStarHeuristics.constantHeuristic();
				heuristic.put(v, constantHeuristicValue);
				break;
			case RANDOM:
				double randomHeuristicValue = AStarHeuristics.randomHeuristic();
				heuristic.put(v, randomHeuristicValue);
				break;
		}
	}

	private static Vertex getMinCostVertex(List<Vertex> openList, 
			Map<Vertex, Double> costSoFar, Map<Vertex, Double> heuristic) {
		Vertex v = null;
		double totalCost = Double.MAX_VALUE;
		
		for (Vertex a: openList) {
			double costSoFarA = costSoFar.get(a);
			double heuristicA = heuristic.get(a);

			if (costSoFarA + heuristicA < totalCost) {
				v = a;
				totalCost = costSoFarA + heuristicA;
			}
		}
		
		
		return v;
	}
	
	private static double getMinCostSoFar(List<Vertex> openList, Map<Vertex, Double> costSoFar) {
		double highestCost = Double.MAX_VALUE;
		
		for (Vertex a: openList) {
			double costSoFarA = costSoFar.get(a);

			if (costSoFarA < highestCost) {
				highestCost = costSoFarA;
			}
		}
		
		
		return highestCost;
	}
	
	private static Path generatePath(Map<Vertex, Vertex> predecessor, Vertex goal) {
		Path p = new Path();
		Vertex current = goal;
		
		while (predecessor.get(current) != null) {
			p.addVertexToBeginning(current);
			current = predecessor.get(current);
		}
		
		return p;
	}
	
	private static double generateTotalCost(Vertex start, Path path, Graph g) {
		double cost = 0.0;
		List<Vertex> pathList = path.getVertices();
		
		if (!pathList.isEmpty()) {
			for (int i = 0, j = 1; j < pathList.size(); i++, j++) {
				//Find the edge along the vertices in the path
				Edge e = g.getEdgeFromTwoVertices(pathList.get(i), pathList.get(j));
				//Add cost of edge to total cost
				cost += e.getCost();
			}
			
			//Add cost from start vertex to first vertex in path
			cost += g.getEdgeFromTwoVertices(start, pathList.get(0)).getCost();
		}
		
		return cost;
	}
}