import processing.core.PVector;


public class Vector {
	private float x;
	private float z;
	private static final double EPSILON = 1e-5;
	
	public Vector(float x, float z) {
		this.setX(x);
		this.z = z;
	}
	
	public Vector add(Vector other) {
		return new Vector(this.x + other.x, this.z + other.z);
	}
	
	public void addU(Vector other) {
		this.x += other.x;
		this.z += other.z;
	}
	
	public String toString() {
		String s = "(";
		s += x + ", ";
		s += z + ")";
		return s;
	}
	
	public Vector scale(int m) {
		return new Vector(m * this.x, m * this.z);
	}
	
	public static double radiansFromVector(PVector v) {
		double radians = Math.atan2(v.x, v.y);
		
		if (radians < 0) {
			radians += Math.PI * 2.0;
		}
		
		return radians;
	}
	
	public static PVector vectorFromRadians(double r) {
		return new PVector((float)Math.sin(r), (float)Math.cos(r));
	}
	
	public static boolean doublesEqual(double d1, double d2) {
		return Math.abs(d1 - d2) <= EPSILON;
	}
	
	public static boolean floatsEqual(float f1, float f2) {
		return Math.abs(f1 - f2) <= EPSILON;
	}
	
	public static double convertToPIRange(double d) {
		while (d > Math.PI) {
			d -= Math.PI * 2;
		}
		
		while (d < -Math.PI) {
			d += Math.PI * 2;
		}
		
		return d;
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getZ() {
		return z;
	}

	public void setZ(float z) {
		this.z = z;
	}
}
