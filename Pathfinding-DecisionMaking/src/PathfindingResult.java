public class PathfindingResult {
	/** The path from the start vertex to the goal vertex **/
	public Path path;
	
	/** If the path is null, this message should explain why **/
	public String message;
	
	/** The cost of traversing the path from start to goal **/
	public double cost;
	
	/** The amount of vertices visited to find the path **/
	public int numVerticesVisited;
	
	/** The amount of time it took to pathfind, in milliseconds **/
	public long runtime;
	
	/** The amount of memory that was used to pathfind **/
	public long usedMemory;
}