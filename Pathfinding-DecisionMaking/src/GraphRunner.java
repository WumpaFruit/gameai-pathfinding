import java.io.*;
import java.text.DecimalFormat;
import java.util.List;


public class GraphRunner {
	
	private static final String DEFAULT_SMALL_FROM = "Coliseum Pay Lot";
	private static final String DEFAULT_SMALL_TO = "Partners Way Parking Deck";
	
	private static final String DEFAULT_LARGE_FROM = "1";
	private static final String DEFAULT_LARGE_TO = "10000";

	public static void main(String[] args) {
		if (args.length == 0) {
			smallGraphDijkstra(null, null);
			DIMACSDijkstra(null, null);
			//smallGraphAStar(null, null, AStarHeuristics.Type.EUCLIDEAN);
			smallGraphAStar(null, null, AStarHeuristics.Type.HAVERSINE);
			//smallGraphAStar(null, null, AStarHeuristics.Type.HAVERSINETRIPLED);
			//smallGraphAStar(null, null, AStarHeuristics.Type.CONSTANT);
			//smallGraphAStar(null, null, AStarHeuristics.Type.RANDOM);
			DIMACSAStar(null, null, AStarHeuristics.Type.EUCLIDEAN);
			//DIMACSAStar(null, null, AStarHeuristics.Type.HAVERSINE);
			//DIMACSAStar(null, null, AStarHeuristics.Type.HAVERSINETRIPLED);
			//DIMACSAStar(null, null, AStarHeuristics.Type.CONSTANT);
			//DIMACSAStar(null, null, AStarHeuristics.Type.RANDOM);
			
		} else {
			runWithOptions(args);
		}
	}
	
	private static void runWithOptions(String[] args) {
		String graph = "";
		String algorithm = "";
		String heuristic = "";
		String start = "";
		String end = "";
		
		graph = args[0];
		algorithm = args[1];
		
		if (algorithm.equalsIgnoreCase("dijkstra")) {
			start = args[2];
			
			if (args.length == 4) {
				end = args[3];
				if (graph.equalsIgnoreCase("small")) {
					smallGraphDijkstra(start, end);
				} else if (graph.equalsIgnoreCase("large")) {
					DIMACSDijkstra(start, end);
				}
			} else {
				if (graph.equalsIgnoreCase("small")) {
					smallGraphDijkstraAll(start);
				} else if (graph.equalsIgnoreCase("large")) {
					DIMACSDijkstraAll(start);
				}
			}
		} else if (algorithm.equalsIgnoreCase("astar") || algorithm.equalsIgnoreCase("A*")) {
			heuristic = args[2];
			start = args[3];
			end = args[4];
			
			if (graph.equalsIgnoreCase("small")) {
				if (heuristic.equalsIgnoreCase("euclidean")) {
					smallGraphAStar(start, end, AStarHeuristics.Type.EUCLIDEAN);
				} else if (heuristic.equalsIgnoreCase("manhattan")) {
					smallGraphAStar(start, end, AStarHeuristics.Type.MANHATTAN);
				} else if (heuristic.equalsIgnoreCase("haversine")) {
					smallGraphAStar(start, end, AStarHeuristics.Type.HAVERSINE);
				} else if (heuristic.equalsIgnoreCase("haversinetripled")) {
					smallGraphAStar(start, end, AStarHeuristics.Type.HAVERSINETRIPLED);
				} else if (heuristic.equalsIgnoreCase("constant")) {
					smallGraphAStar(start, end, AStarHeuristics.Type.CONSTANT);
				} else if (heuristic.equalsIgnoreCase("random")) {
					smallGraphAStar(start, end, AStarHeuristics.Type.RANDOM);
				}
			} else if (graph.equalsIgnoreCase("large")) {
				if (heuristic.equalsIgnoreCase("euclidean")) {
					DIMACSAStar(start, end, AStarHeuristics.Type.EUCLIDEAN);
				} else if (heuristic.equalsIgnoreCase("manhattan")) {
					DIMACSAStar(start, end, AStarHeuristics.Type.MANHATTAN);
				} else if (heuristic.equalsIgnoreCase("haversine")) {
					DIMACSAStar(start, end, AStarHeuristics.Type.HAVERSINE);
				} else if (heuristic.equalsIgnoreCase("haversinetripled")) {
					DIMACSAStar(start, end, AStarHeuristics.Type.HAVERSINETRIPLED);
				} else if (heuristic.equalsIgnoreCase("constant")) {
					DIMACSAStar(start, end, AStarHeuristics.Type.CONSTANT);
				} else if (heuristic.equalsIgnoreCase("random")) {
					DIMACSAStar(start, end, AStarHeuristics.Type.RANDOM);
				}
			}
		}
	}
	
	private static void smallGraphDijkstra(String from, String to) {
		boolean needToSerialize = false;
		System.out.println("Performing Dijkstra's Algorithm on the small graph");
		
		if (from == null) {
			from = DEFAULT_SMALL_FROM;
		}
		
		if (to == null) {
			to = DEFAULT_SMALL_TO;
		}
		
		System.out.println("Creating graph. This may take a minute...");
		Graph g = deserializeGraph("smallgraph.ser");
		
		if (g == null) {
			g = Graph.createGraph("smallgraph.graph");
			needToSerialize = true;
		}
		
		System.out.println("Graph created.");
		System.out.println("Adding coordinate information. This may take a minute...");
		g.addLatitudeLongitudeInfo("smallgraphcoords.aux");
		
		System.out.println("Coordinate information added.");
		
		System.out.println("Finding a path from " + from + " to " + to);
		printResults(Dijkstra.pathfindDijkstra(g, new Vertex(from), new Vertex(to)), g);
		if (needToSerialize) {
			serializeGraph(g, "smallgraph.ser");
		}
	}
	
	private static void smallGraphAStar(String from, String to, AStarHeuristics.Type type) {
		boolean needToSerialize = false;
		System.out.println("Performing A* Algorithm on the small graph");
		
		if (from == null) {
			from = DEFAULT_SMALL_FROM;
		}
		
		if (to == null) {
			to = DEFAULT_SMALL_TO;
		}
		
		System.out.println("Creating graph. This may take a minute...");
		Graph g = deserializeGraph("smallgraph.ser");
		
		if (g == null) {
			g = Graph.createGraph("smallgraph.graph");
			needToSerialize = true;
		}
		
		System.out.println("Graph created.");
		System.out.println("Adding coordinate information. This may take a minute...");
		g.addLatitudeLongitudeInfo("smallgraphcoords.aux");
		
		System.out.println("Coordinate information added.");
		
		System.out.println("Finding a path from " + from + " to " + to);
		printResults(AStar.pathfindAStar(g, new Vertex(from), new Vertex(to), type, true), g);
		if (needToSerialize) {
			serializeGraph(g, "smallgraph.ser");
		}
	}
	
	private static void DIMACSDijkstraAll(String from) {
		if (from == null) {
			from = DEFAULT_LARGE_FROM;
		}
		
		System.out.println("Creating graph. This may take a minute...");
		DIMACSGraph g = DIMACSGraph.createGraphFromGR("USA-road-d.NY.gr");
		
		System.out.println("Graph created.");
		System.out.println("Adding coordinate information. This may take a minute...");
		g.addCoordInfo("USA-road-d.NY.co");
		
		System.out.println("Coordinate information added.");
		
		printResultsList(Dijkstra.pathfindDijkstra(g, new Vertex(from)), g);
	}
	
	private static void DIMACSDijkstra(String from, String to) {
		boolean needToSerialize = false;
		
		if (from == null) {
			from = DEFAULT_LARGE_FROM;
		}
		
		if (to == null) {
			to = DEFAULT_LARGE_TO;
		}
		
		System.out.println("Creating graph. This may take a minute...");
		DIMACSGraph g = (DIMACSGraph)deserializeGraph("USA-road-d.NY.ser");
		
		if (g == null) {
			g = DIMACSGraph.createGraphFromGR("USA-road-d.NY.gr");
			needToSerialize = true;
		}
		
		System.out.println("Graph created.");
		System.out.println("Adding coordinate information. This may take a minute...");
		g.addCoordInfo("USA-road-d.NY.co");
		
		System.out.println("Coordinate information added.");
		
		printResults(Dijkstra.pathfindDijkstra(g, new Vertex(from), new Vertex(to)), g);
		
		if (needToSerialize) {
			serializeGraph(g, "USA-road-d.NY.ser");
		}
	}
	
	private static void smallGraphDijkstraAll(String from) {
		if (from == null) {
			from = DEFAULT_SMALL_FROM;
		}
		
		System.out.println("Creating graph. This may take a minute...");
		Graph g = Graph.createGraph("smallgraph.graph");
		
		System.out.println("Graph created.");
		System.out.println("Adding coordinate information. This may take a minute...");
		g.addLatitudeLongitudeInfo("smallgraphcoords.aux");
		
		System.out.println("Coordinate information added.");
		
		printResultsList(Dijkstra.pathfindDijkstra(g, new Vertex(from)), g);
	}
	
	private static void DIMACSAStar(String from, String to, AStarHeuristics.Type type) {
		boolean needToSerialize = false;
		
		if (from == null) {
			from = DEFAULT_LARGE_FROM;
		}
		
		if (to == null) {
			to = DEFAULT_LARGE_TO;
		}
		
		System.out.println("Creating graph. This may take a minute...");
		DIMACSGraph g = (DIMACSGraph)deserializeGraph("USA-road-d.NY.ser");
		
		if (g == null) {
			g = DIMACSGraph.createGraphFromGR("USA-road-d.NY.gr");
			needToSerialize = true;
		}
		
		System.out.println("Graph created.");
		System.out.println("Adding coordinate information. This may take a minute...");
		g.addCoordInfo("USA-road-d.NY.co");
		
		System.out.println("Coordinate information added.");
		
		printResults(AStar.pathfindAStar(g, new Vertex(from), new Vertex(to), type, false), g);
		
		if (needToSerialize) {
			serializeGraph(g, "USA-road-d.NY.ser");
		}
	}
	
	private static void printResultsList(List<PathfindingResult> results, Graph g) {
		DecimalFormat formatter = new DecimalFormat("#,###");
		long totalRuntime = 0;
		for (PathfindingResult result: results) {
			
			printResults(result, g);
			totalRuntime += result.runtime;
		}
		System.out.println("Total runtime: " + formatter.format(totalRuntime) + " nanoseconds/" + formatter.format(totalRuntime * Math.pow(10, -6)) + " millisecond(s)/" + formatter.format(totalRuntime * Math.pow(10, -9)) + " second(s)");
	}
	
	private static void printResults(PathfindingResult result, Graph g) {
		if (result.path != null) {
			DecimalFormat formatter = new DecimalFormat("#,###");
			System.out.println("\nPath: " + result.path);
			System.out.printf("Total cost: %.2f\n", result.cost);
			System.out.printf("Fill size: %d (%.2f%%)\n", result.numVerticesVisited, ((double)result.numVerticesVisited/g.getNumVertices() * 100));
			System.out.println("Runtime: " + formatter.format(result.runtime) + " nanoseconds/" + formatter.format(result.runtime * Math.pow(10, -6)) + " millisecond(s)/" + formatter.format(result.runtime * Math.pow(10, -9)) + " second(s)");
			System.out.println("Used memory: " + formatter.format(result.usedMemory) + " bytes/" + formatter.format(result.usedMemory * Math.pow(2, -10)) + " kilobytes/" + formatter.format(result.usedMemory * Math.pow(2, -20)) + " megabytes");
		} else {
			System.err.println("There was an error finding a path: " + result.message);
		}
	}
	
	public static void serializeGraph(Graph g, String filename) {
		try (OutputStream file = new FileOutputStream(filename);
			 OutputStream buffer = new BufferedOutputStream(file);
		     ObjectOutput output = new ObjectOutputStream(buffer);) {
			output.writeObject(g);
		} catch (FileNotFoundException e) {
			return;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static Graph deserializeGraph(String filename) {
		try (InputStream file = new FileInputStream(filename);
			 InputStream buffer = new BufferedInputStream(file);
			 ObjectInput input = new ObjectInputStream(buffer);) {
			return (Graph)input.readObject();
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return null;
	}
}
