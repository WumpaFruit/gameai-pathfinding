import java.io.Serializable;


public class Edge implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4203460323204056137L;
	private double cost;
	private Vertex fromVertex, toVertex;
	
	public Edge(Vertex from, Vertex to, double cost) {
		fromVertex = from;
		toVertex = to;
		this.cost = cost;
	}
	
	public double getCost() {
		return cost;
	}
	
	public Vertex getFromVertex() {
		return fromVertex;
	}
	
	public Vertex getToVertex() {
		return toVertex;
	}

	@Override
	public String toString() {
		return "[" + fromVertex + "--" + cost + "-->" + toVertex + "]";
	}
	
}
